import { writable, type Writable } from "svelte/store";

export const creatorFormVisible: Writable<boolean> = writable(false);
export const currentlyEditedCase: Writable<number> = writable(0);

export const closeCreatorForm = () => creatorFormVisible.set(false);
export const openCreatorForm = () => creatorFormVisible.set(true);

creatorFormVisible.subscribe(visibility => {
    if (!visibility) {
        document.body.style.position = '';
        currentlyEditedCase.set(0);
    } else document.body.style.position = 'fixed';
})

currentlyEditedCase.subscribe(id => {
    if (id > 0) openCreatorForm();
})