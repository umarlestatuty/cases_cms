import Auth from "@lib/Auth";
import env from "@lib/env";

export default class REST {
    private basicAuthCredentials: string;
    public coreRESTUrl: string;

    constructor() {
        const CredentialsManager: Auth = new Auth();
        this.basicAuthCredentials = CredentialsManager.getCredentials().basicAuth;
        this.coreRESTUrl = env.REST_CORE_URL;        
    }

    private getRequestHeaders(json: boolean = false): Headers {
        const requestHeaders: Headers = new Headers();
        requestHeaders.append('Authorization', this.basicAuthCredentials);
        if (json) requestHeaders.append("Content-Type", "application/json");

        return requestHeaders;
    }

    private getRequestEndpoint(path: string): string {
        return `https://api.spis.umarlestatuty.pl${path}`;
    }

    public request = {
        credentials: {
            verify: async () => {
                const response = await fetch(this.getRequestEndpoint('/auth/verify'), { headers: this.getRequestHeaders() });
                const verifyResult = await response.json();
                return verifyResult.success ? verifyResult.success : false;
            }
        },

        cases: {
            get: async (keywords: string = '') => {
                const response = await fetch(this.getRequestEndpoint(`/cases?keywords=${keywords}`));
                return await response.json();
            },

            doesExist: async (caseId: string = '') => {
                const response = await fetch(this.getRequestEndpoint(`/cases/doesExist?caseId=${caseId}`));
                return await response.json();
            },

            update: async (caseData) => {
                const response = await fetch(this.getRequestEndpoint('/cases'), { body: JSON.stringify(caseData), method: 'POST', headers: this.getRequestHeaders(true) });
                return await response.json();
            }
        }
    }
}